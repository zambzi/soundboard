from django.contrib import admin
from boardapp import models

class SoundAdmin(admin.ModelAdmin):
	list_display = ('name', 'tags', 'created_at')
	ordering = ('-created_at',)

admin.site.register(models.Sound, SoundAdmin)
admin.site.register(models.UserCode, admin.ModelAdmin)
admin.site.register(models.Favorite, admin.ModelAdmin)