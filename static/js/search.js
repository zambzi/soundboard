function createTemplate( name, file, icon, counter ){
	return "<div class=\"sound\" style=\"background-image: url('"+ icon +"')\">\
		        <div class=\"name\">"+ name +"</div>\
		        <div class=\"player\">\
		            <audio id=\"player-"+ counter +"\" src=\""+ file +"\"></audio>\
		            <div class=\"controls\">\
		                <button onclick=\"\
		                    document.getElementById('player-"+ counter +"').play();\
		                    document.getElementById('player-"+ counter +"').currentTime = 0;\">PLAY</button>\
		                <button onclick=\"\
		                    document.getElementById('player-"+ counter +"').pause();\
		                    document.getElementById('player-"+ counter +"').currentTime = 0;\">STOP</button>\
		            </div>\
		        </div>\
		    </div>"
}

function addEvent(element, name, callback) {
    if (element.addEventListener) {
        element.addEventListener(name, callback, false);
    } else if (element.attachEvent) {
        element.attachEvent("on" + eventName, callback);
    }
}

function onResponse( responseString ){
	var response = JSON.parse( responseString );
	if( typeof response == "undefined" )
		return;
	if( typeof response.message == "undefined" )
		return;
	if( response.message == "empty" || response.message == "error" )
		return;//add message on screen?
	if( typeof response.data == "undefined" )
		return;
	var container = document.getElementById("soundlist");
	container.innerHTML = "";
	for (var i = response.data.length - 1; i >= 0; i--) {
		var sound = response.data[i];
		var template = createTemplate(sound.name, sound.file, sound.icon, i);
		container.innerHTML += template;
	};
}

function searchQuery( query ){
    var xmlhttp;
    //No one gives a shit about IE6
    xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == XMLHttpRequest.DONE ) {
           if(xmlhttp.status == 200){
           		onResponse(xmlhttp.responseText)
           }
           else if(xmlhttp.status == 400) {
              console.log('There was an error 400');
           }
           else {
               console.log('something else other than 200 was returned');
           }
        }
    };

    xmlhttp.open("GET", "/search_query?type=search&query=" + query, true);
    xmlhttp.send();
}

function registerSearchEvents(){
	var searchField = document.getElementById("search-field");
	addEvent( searchField, "keydown", function(){
		if( searchField.value.length >= 3 || searchField.value.length <= 0 )
			searchQuery( searchField.value );
	} )
	searchQuery( "" );
}

document.addEventListener("load", registerSearchEvents());